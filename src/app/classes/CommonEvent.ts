import {IEventOptions} from '../interfaces/IEventOptions';
import {IEventDescription} from '../interfaces/IEventDescription';


export class CommonEvent {
  private _eventId: number|null;
  private _type: string;
  private _animalId: string;
  private _cowId: number|null;
  private _deletable: boolean;
  private _lactationNumber: number|null;
  private _daysInLactation: number|null;
  private _ageInDays: number|null;
  private _startDateTime: number|null;
  private _reportingDateTime: number|null;
  protected eventDescription: IEventDescription|null;
  static getEventIconClass(): string {
    return '';
  }
  static getEventName(): string {
    return '';
  }
  constructor(
    options: IEventOptions,
    createEmpty: boolean = false,
  ) {
    this._eventId = !createEmpty ? options.eventId : null;
    this._type = !createEmpty ? options.type : null;
    this._animalId = !createEmpty ? options.animalId : null;
    this._cowId = !createEmpty ? options.cowId : null;
    this._deletable = !createEmpty ? options.deletable : null;
    this._lactationNumber = !createEmpty ? options.lactationNumber : null;
    this._daysInLactation = !createEmpty ? options.daysInLactation : null;
    this._ageInDays = !createEmpty ? options.ageInDays : null;
    this._startDateTime = !createEmpty ? options.startDateTime : null;
    this._reportingDateTime = !createEmpty ? options.reportingDateTime : null;
  }

  get description(): IEventDescription {
    return {
      comment: '...',
      value: null,
    };
  }

  get eventId(): number {
    return this._eventId;
  }

  set eventId(value: number) {
    this._eventId = value;
  }

  get type(): string {
    return this._type;
  }

  set type(value: string) {
    this._type = value;
  }

  get animalId(): string {
    return this._animalId;
  }

  set animalId(value: string) {
    this._animalId = value;
  }

  get cowId(): number {
    return this._cowId;
  }

  set cowId(value: number) {
    this._cowId = value;
  }

  get deletable(): boolean {
    return this._deletable;
  }

  set deletable(value: boolean) {
    this._deletable = value;
  }

  get lactationNumber(): number {
    return this._lactationNumber;
  }

  set lactationNumber(value: number) {
    this._lactationNumber = value;
  }

  get daysInLactation(): number {
    return this._daysInLactation;
  }

  set daysInLactation(value: number) {
    this._daysInLactation = value;
  }

  get ageInDays(): number {
    return this._ageInDays;
  }

  set ageInDays(value: number) {
    this._ageInDays = value;
  }

  get startDateTime(): number {
    return this._startDateTime;
  }

  set startDateTime(value: number) {
    this._startDateTime = value;
  }

  get reportingDateTime(): number {
    return this._reportingDateTime;
  }

  set reportingDateTime(value: number) {
    this._reportingDateTime = value;
  }
}
