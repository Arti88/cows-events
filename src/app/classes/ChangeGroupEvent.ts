import {CommonEvent} from './CommonEvent';
import {IEventOptions} from '../interfaces/IEventOptions';
import {IEventDescription} from '../interfaces/IEventDescription';

export class ChangeGroupEvent extends CommonEvent {
  private _newGroupId: number|null;
  private _newGroupName: string|null;
  private _currentGroupId: number|null;
  private _currentGroupName: string|null;
  static getEventIconClass(): string {
    return 'dolly-flatbed';
  }
  static getEventName(): string {
    return 'Change group';
  }
  constructor(options: IEventOptions, createEmpty = false) {
    super(options, createEmpty);
    this._newGroupId = !createEmpty ? options.newGroupId : null;
    this._newGroupName = !createEmpty ? options.newGroupName : null;
    this._currentGroupId = !createEmpty ? options.currentGroupId : null;
    this._currentGroupName = !createEmpty ? options.currentGroupName : null;
  }

  get description(): IEventDescription {
    return {
      comment: 'New group',
      value: this.newGroupName,
    };
  }

  get newGroupId(): number {
    return this._newGroupId;
  }

  set newGroupId(value: number) {
    this._newGroupId = value;
  }

  get newGroupName(): string {
    return this._newGroupName;
  }

  set newGroupName(value: string) {
    this._newGroupName = value;
  }

  get currentGroupId(): number {
    return this._currentGroupId;
  }

  set currentGroupId(value: number) {
    this._currentGroupId = value;
  }

  get currentGroupName(): string {
    return this._currentGroupName;
  }

  set currentGroupName(value: string) {
    this._currentGroupName = value;
  }
}
