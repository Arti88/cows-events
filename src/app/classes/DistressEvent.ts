import {CommonEvent} from './CommonEvent';
import {IEventOptions} from '../interfaces/IEventOptions';
import {IEventDescription} from '../interfaces/IEventDescription';

export class DistressEvent extends CommonEvent {
  private _alertType: string|null;
  private _duration: number|null;
  private _originalStartDateTime: number|null;
  private _endDateTime: number|null;
  private _daysInPregnancy: number|null;
  static getEventIconClass(): string {
    return 'kiss';
  }
  static getEventName(): string {
    return 'Distress';
  }
  constructor(options: IEventOptions, createEmpty = false) {
    super(options, createEmpty);
    this._alertType = !createEmpty ? options.alertType : null;
    this._duration = !createEmpty ? options.duration : null;
    this._originalStartDateTime = !createEmpty ? options.originalStartDateTime : null;
    this._endDateTime = !createEmpty ? options.endDateTime : null;
    this._daysInPregnancy = !createEmpty ? options.daysInPregnancy : null;
  }

  get description(): IEventDescription {
    return {
      comment: 'Duration',
      value: this.duration,
    };
  }

  get alertType(): string {
    return this._alertType;
  }

  set alertType(value: string) {
    this._alertType = value;
  }

  get duration(): number {
    return this._duration;
  }

  set duration(value: number) {
    this._duration = value;
  }

  get originalStartDateTime(): number | null {
    return this._originalStartDateTime;
  }

  set originalStartDateTime(value: number | null) {
    this._originalStartDateTime = value;
  }

  get endDateTime(): number | null {
    return this._endDateTime;
  }

  set endDateTime(value: number | null) {
    this._endDateTime = value;
  }

  get daysInPregnancy(): number | null {
    return this._daysInPregnancy;
  }

  set daysInPregnancy(value: number | null) {
    this._daysInPregnancy = value;
  }
}
