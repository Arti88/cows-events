import {CommonEvent} from './CommonEvent';
import {IEventOptions} from '../interfaces/IEventOptions';
import {IEventDescription} from '../interfaces/IEventDescription';

export class CalvingEvent extends CommonEvent {
  private _destinationGroup: number|null;
  private _destinationGroupName: string|null;
  private _daysInPregnancy: number|null;
  private _oldLactationNumber: number|null;
  private _newborns: unknown|null;
  static getEventIconClass(): string {
    return 'democrat';
  }
  static getEventName(): string {
    return 'Calving';
  }
  constructor(options: IEventOptions, createEmpty = false) {
    super(options, createEmpty);
    this._destinationGroup = !createEmpty ? options.destinationGroup : null;
    this._destinationGroupName = !createEmpty ? options.destinationGroupName : null;
    this._daysInPregnancy = !createEmpty ? options.daysInPregnancy : null;
    this._oldLactationNumber = !createEmpty ? options.oldLactationNumber : null;
    this._newborns = !createEmpty ? options.newborns : null;
  }

  get description(): IEventDescription {
    return {
      comment: 'New lactation number',
      value: this.lactationNumber,
    };
  }

  get destinationGroup(): number {
    return this._destinationGroup;
  }

  set destinationGroup(value: number) {
    this._destinationGroup = value;
  }

  get destinationGroupName(): string {
    return this._destinationGroupName;
  }

  set destinationGroupName(value: string) {
    this._destinationGroupName = value;
  }

  get daysInPregnancy(): number {
    return this._daysInPregnancy;
  }

  set daysInPregnancy(value: number) {
    this._daysInPregnancy = value;
  }

  get oldLactationNumber(): number {
    return this._oldLactationNumber;
  }

  set oldLactationNumber(value: number) {
    this._oldLactationNumber = value;
  }

  get newborns(): unknown {
    return this._newborns;
  }

  set newborns(value: unknown) {
    this._newborns = value;
  }
}
