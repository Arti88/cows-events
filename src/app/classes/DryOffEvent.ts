import {CommonEvent} from './CommonEvent';
import {IEventOptions} from '../interfaces/IEventOptions';
import {IEventDescription} from '../interfaces/IEventDescription';

export class DryOffEvent extends CommonEvent {
  private _destinationGroup: number|null;
  private _destinationGroupName: string|null;
  private _daysInPregnancy: number|null;
  static getEventIconClass(): string {
    return 'java';
  }
  static getEventName(): string {
    return 'Dry off';
  }
  constructor(options: IEventOptions, createEmpty = false) {
    super(options, createEmpty);
    this._destinationGroup = !createEmpty ? options.destinationGroup : null;
    this._destinationGroupName = !createEmpty ? options.destinationGroupName : null;
    this._daysInPregnancy = !createEmpty ? options.daysInPregnancy : null;
  }

  get description(): IEventDescription {
    return {
      comment: 'Days in pregnancy',
      value: this.daysInPregnancy,
    };
  }

  get destinationGroup(): number {
    return this._destinationGroup;
  }

  set destinationGroup(value: number) {
    this._destinationGroup = value;
  }

  get destinationGroupName(): string {
    return this._destinationGroupName;
  }

  set destinationGroupName(value: string) {
    this._destinationGroupName = value;
  }

  get daysInPregnancy(): number {
    return this._daysInPregnancy;
  }

  set daysInPregnancy(value: number) {
    this._daysInPregnancy = value;
  }
}
