import {CommonEvent} from './CommonEvent';
import {IEventOptions} from '../interfaces/IEventOptions';
import {IEventDescription} from '../interfaces/IEventDescription';

export class BirthEvent extends CommonEvent {
  private _birthDateCalculated: boolean;
  static getEventIconClass(): string {
    return 'plus-square';
  }
  static getEventName(): string {
    return 'Birth';
  }
  constructor(options: IEventOptions, createEmpty = false) {
    super(options, createEmpty);
    this._birthDateCalculated = !createEmpty ? options.birthDateCalculated : false;
  }
  get birthDateCalculated(): boolean {
    return this._birthDateCalculated;
  }

  set birthDateCalculated(value: boolean) {
    this._birthDateCalculated = value;
  }

  get description(): IEventDescription {
    return {
      comment: 'Birth date has been calculated',
      value: this.birthDateCalculated,
    };
  }
}
