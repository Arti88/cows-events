import {CommonEvent} from './CommonEvent';
import {IEventOptions} from '../interfaces/IEventOptions';
import {IEventDescription} from '../interfaces/IEventDescription';

export class SystemHeatEvent extends CommonEvent {
  private _heatIndexPeak: string|null;
  static getEventIconClass(): string {
    return 'chevron-up';
  }
  static getEventName(): string {
    return 'System heat';
  }
  constructor(options: IEventOptions, createEmpty = false) {
    super(options, createEmpty);
    this._heatIndexPeak = !createEmpty ? options.heatIndexPeak : null;
  }

  get description(): IEventDescription {
    return {
      comment: 'Heat index',
      value: this.heatIndexPeak,
    };
  }

  get heatIndexPeak(): string {
    return this._heatIndexPeak;
  }

  set heatIndexPeak(value: string) {
    this._heatIndexPeak = value;
  }
}
