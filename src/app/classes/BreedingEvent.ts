import {CommonEvent} from './CommonEvent';
import {IEventOptions} from '../interfaces/IEventOptions';
import {IEventDescription} from '../interfaces/IEventDescription';

export class BreedingEvent extends CommonEvent {
  private _sire: unknown|null;
  private _breedingNumber: number|null;
  private _isOutOfBreedingWindow: boolean|null;
  private _interval: number|null;
  static getEventIconClass(): string {
    return 'glass-cheers';
  }
  static getEventName(): string {
    return 'Breeding';
  }
  constructor(options: IEventOptions, createEmpty = false) {
    super(options, createEmpty);
    this._sire = !createEmpty ? options.sire : null;
    this._breedingNumber = !createEmpty ? options.breedingNumber : null;
    this._isOutOfBreedingWindow = !createEmpty ? options.isOutOfBreedingWindow : null;
    this._interval = !createEmpty ? options.interval : null;
  }

  get description(): IEventDescription {
    return {
      comment: 'Breeding number',
      value: this.breedingNumber,
    };
  }

  get sire(): unknown {
    return this._sire;
  }

  set sire(value: unknown) {
    this._sire = value;
  }

  get breedingNumber(): number {
    return this._breedingNumber;
  }

  set breedingNumber(value: number) {
    this._breedingNumber = value;
  }

  get isOutOfBreedingWindow(): boolean {
    return this._isOutOfBreedingWindow;
  }

  set isOutOfBreedingWindow(value: boolean) {
    this._isOutOfBreedingWindow = value;
  }

  get interval(): number {
    return this._interval;
  }

  set interval(value: number) {
    this._interval = value;
  }
}
