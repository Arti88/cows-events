import {CommonEvent} from './CommonEvent';
import {IEventOptions} from '../interfaces/IEventOptions';
import {IEventDescription} from '../interfaces/IEventDescription';

export class SystemHealthEvent extends CommonEvent {
  private _healthIndex: number|null;
  private _endDate: number|null;
  private _minValueDateTime: number|null;
  static getEventIconClass(): string {
    return 'chart-line';
  }
  static getEventName(): string {
    return 'System health';
  }
  constructor(options: IEventOptions, createEmpty = false) {
    super(options, createEmpty);
    this._healthIndex = !createEmpty ? options.healthIndex : null;
    this._endDate = !createEmpty ? options.endDate : null;
    this._minValueDateTime = !createEmpty ? options.minValueDateTime : null;
  }

  get description(): IEventDescription {
    return {
      comment: 'System health',
      value: this.healthIndex,
    };
  }

  get healthIndex(): number {
    return this._healthIndex;
  }

  set healthIndex(value: number) {
    this._healthIndex = value;
  }

  get endDate(): number {
    return this._endDate;
  }

  set endDate(value: number) {
    this._endDate = value;
  }

  get minValueDateTime(): number {
    return this._minValueDateTime;
  }

  set minValueDateTime(value: number) {
    this._minValueDateTime = value;
  }
}
