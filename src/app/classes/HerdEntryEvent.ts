import {CommonEvent} from './CommonEvent';
import {IEventOptions} from '../interfaces/IEventOptions';
import {IEventDescription} from '../interfaces/IEventDescription';

export class HerdEntryEvent extends CommonEvent {
  private _destinationGroup: number|null;
  private _destinationGroupName: string|null;
  private _cowEntryStatus: string|null;
  static getEventIconClass(): string {
    return 'lemon';
  }
  static getEventName(): string {
    return 'Herd entry';
  }
  constructor(options: IEventOptions, createEmpty = false) {
    super(options, createEmpty);
    this._destinationGroup = !createEmpty ? options.destinationGroup : null;
    this._destinationGroupName = !createEmpty ? options.destinationGroupName : null;
    this._cowEntryStatus = !createEmpty ? options.cowEntryStatus : null;
  }

  get description(): IEventDescription {
    return {
      comment: 'Status',
      value: this.cowEntryStatus,
    };
  }

  get destinationGroup(): number {
    return this._destinationGroup;
  }

  set destinationGroup(value: number) {
    this._destinationGroup = value;
  }

  get destinationGroupName(): string | null {
    return this._destinationGroupName;
  }

  set destinationGroupName(value: string | null) {
    this._destinationGroupName = value;
  }

  get cowEntryStatus(): string {
    return this._cowEntryStatus;
  }

  set cowEntryStatus(value: string) {
    this._cowEntryStatus = value;
  }
}
