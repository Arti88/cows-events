import { Component, OnInit } from '@angular/core';
import {EventsService} from '../events.service';
import {IEventsDataResponse} from '../interfaces/IEventsDataResponse';
import {CommonEvent} from '../classes/CommonEvent';
import {IEventOptions} from '../interfaces/IEventOptions';
import {EventFactory} from '../factories/EventFactory';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {EventModalComponent} from '../event-modal/event-modal.component';
import {CreateEventModalComponent} from '../create-event-modal/create-event-modal.component';

@Component({
  selector: 'app-events-table',
  templateUrl: './events-table.component.html',
  styleUrls: ['./events-table.component.css']
})
export class EventsTableComponent implements OnInit {
  offset: number;
  limit: number;
  total: number;
  data: CommonEvent[] = [];
  isLoading = false;

  constructor(
    private eventsService: EventsService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.getData();
  }

  protected addEvent(): void {
    const modalRef = this.modalService.open(CreateEventModalComponent);
    modalRef.result.then((result) => {
      if (result instanceof CommonEvent) {
        result.eventId = this.data[0]
          ? this.data[0].eventId + 1
          : 1;
        this.data.unshift(result);
      }
    });
  }

  protected editData(eventData: CommonEvent, index: number): void {
    const modalRef = this.modalService.open(EventModalComponent);
    modalRef.componentInstance.eventData = Object.assign(Object.create(Object.getPrototypeOf(eventData)), eventData);
    modalRef.result.then((result) => {
      if (result === false) {
        this.deleteEventByIndex(index);
        return;
      }
      if (result instanceof CommonEvent) {
        this.saveEventData(result, index);
      }
    });
  }

  private getData() {
    this.isLoading = true;
    this.eventsService.getData().subscribe((response: IEventsDataResponse) => {
      this.offset = response.offset;
      this.limit = response.limit;
      this.total = response.total;
      this.data = response.result.map((eventOptions: IEventOptions) => EventFactory.createFromOptions(eventOptions));
      this.isLoading = false;
    });
  }

  private deleteEventByIndex(eventIndex: number) {
    this.isLoading = true;
    this.data.splice(eventIndex, 1);
    this.isLoading = false;
  }

  private saveEventData(eventData: CommonEvent, index: number): void {
    this.isLoading = true;
    this.data[index] = eventData;
    this.isLoading = false;
  }
}
