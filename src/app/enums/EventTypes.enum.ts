export enum EventTypes {
  NOT_SELECTED = '<not selected>',
  BIRTH = 'birth',
  BREEDING = 'breeding',
  CALVING = 'calving',
  CHANGE_GROUP = 'changeGroup',
  DISTRESS = 'distress',
  DRY_OFF = 'dryOff',
  HERD_ENTRY = 'herdEntry',
  SYSTEM_HEALTH = 'systemHealth',
  SYSTEM_HEAT = 'systemHeat',
}
