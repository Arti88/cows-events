import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {EventTypes} from '../enums/EventTypes.enum';
import {BirthEvent} from '../classes/BirthEvent';
import {BreedingEvent} from '../classes/BreedingEvent';
import {CalvingEvent} from '../classes/CalvingEvent';
import {ChangeGroupEvent} from '../classes/ChangeGroupEvent';
import {DistressEvent} from '../classes/DistressEvent';
import {DryOffEvent} from '../classes/DryOffEvent';
import {HerdEntryEvent} from '../classes/HerdEntryEvent';
import {SystemHealthEvent} from '../classes/SystemHealthEvent';
import {SystemHeatEvent} from '../classes/SystemHeatEvent';

@Component({
  selector: 'app-create-event-modal',
  templateUrl: './create-event-modal.component.html',
  styleUrls: ['./create-event-modal.component.css']
})
export class CreateEventModalComponent implements OnInit {
  public eventTypeIsSelected = false;
  public eventTypes: string[] = Object.values(EventTypes);
  public selectedEventType: string = EventTypes.NOT_SELECTED;
  public eventData = null;
  ngOnInit() {

  }

  constructor(
    public activeModal: NgbActiveModal
  ) {}

  onEventTypeChange(eventType: EventTypes) {
    switch (eventType) {
      case EventTypes.NOT_SELECTED:
        this.eventData = null;
        this.eventTypeIsSelected = false;
        break;
      case EventTypes.BIRTH:
        this.eventData = new BirthEvent(null, true);
        this.eventTypeIsSelected = true;
        break;
      case EventTypes.BREEDING:
        this.eventData = new BreedingEvent(null, true);
        this.eventTypeIsSelected = true;
        break;
      case EventTypes.CALVING:
        this.eventData = new CalvingEvent(null, true);
        this.eventTypeIsSelected = true;
        break;
      case EventTypes.CHANGE_GROUP:
        this.eventData = new ChangeGroupEvent(null, true);
        this.eventTypeIsSelected = true;
        break;
      case EventTypes.DISTRESS:
        this.eventData = new DistressEvent(null, true);
        this.eventTypeIsSelected = true;
        break;
      case EventTypes.DRY_OFF:
        this.eventData = new DryOffEvent(null, true);
        this.eventTypeIsSelected = true;
        break;
      case EventTypes.HERD_ENTRY:
        this.eventData = new HerdEntryEvent(null, true);
        this.eventTypeIsSelected = true;
        break;
      case EventTypes.SYSTEM_HEALTH:
        this.eventData = new SystemHealthEvent(null, true);
        this.eventTypeIsSelected = true;
        break;
      case EventTypes.SYSTEM_HEAT:
        this.eventData = new SystemHeatEvent(null, true);
        this.eventTypeIsSelected = true;
        break;
    }
  }

  create() {
    this.activeModal.close(this.eventData);
  }

  close() {
    this.activeModal.close(null);
  }

  isBirthEvent() { return this.eventData instanceof BirthEvent; }
  isBreedingEvent() { return this.eventData instanceof BreedingEvent; }
  isCalvingEvent() { return this.eventData instanceof CalvingEvent; }
  isChangeGroupEvent() { return this.eventData instanceof ChangeGroupEvent; }
  isDistressEvent() { return this.eventData instanceof DistressEvent; }
  isDryOffEvent() { return this.eventData instanceof DryOffEvent; }
  isHerdEntryEvent() { return this.eventData instanceof HerdEntryEvent; }
  isSystemHealthEvent() { return this.eventData instanceof SystemHealthEvent; }
  isSystemHeatEvent() { return this.eventData instanceof SystemHeatEvent; }
}
