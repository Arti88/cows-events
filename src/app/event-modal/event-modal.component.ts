import {Component, EventEmitter, Input, OnInit} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {BirthEvent} from '../classes/BirthEvent';
import {SystemHeatEvent} from '../classes/SystemHeatEvent';
import {BreedingEvent} from '../classes/BreedingEvent';
import {CalvingEvent} from '../classes/CalvingEvent';
import {ChangeGroupEvent} from '../classes/ChangeGroupEvent';
import {DistressEvent} from '../classes/DistressEvent';
import {DryOffEvent} from '../classes/DryOffEvent';
import {HerdEntryEvent} from '../classes/HerdEntryEvent';
import {SystemHealthEvent} from '../classes/SystemHealthEvent';

@Component({
  selector: 'app-event-modal',
  templateUrl: './event-modal.component.html',
  styleUrls: ['./event-modal.component.css']
})
export class EventModalComponent implements OnInit {
  @Input() public eventData;
  ngOnInit() {
    if (this.eventData.minValueDateTime) {
      this.eventData.minValueDateTime = new Date(1000 * this.eventData.minValueDateTime)
        .toISOString().slice(0, 16);
    }
    if (this.eventData.startDateTime) {
      this.eventData.startDateTime = new Date(1000 * this.eventData.startDateTime)
        .toISOString().slice(0, 16);
    }
    if (this.eventData.endDate) {
      this.eventData.endDate = new Date(1000 * this.eventData.endDate)
        .toISOString().slice(0, 16);
    }
  }

  constructor(
    public activeModal: NgbActiveModal
  ) {}

  save() {
    this.eventData.reportingDateTime = Math.ceil(Date.now() / 1000);
    this.eventData.minValueDateTime = this.eventData.minValueDateTime
      ? new Date(this.eventData.minValueDateTime).getTime() / 1000
      : null;
    this.eventData.startDateTime = this.eventData.startDateTime
      ? new Date(this.eventData.startDateTime).getTime() / 1000
      : null;
    this.eventData.endDate = this.eventData.endDate
      ? new Date(this.eventData.endDate).getTime() / 1000
      : null;
    this.activeModal.close(this.eventData);
  }

  delete() {
    this.activeModal.close(false);
  }

  close() {
    this.activeModal.close(null);
  }

  isBirthEvent() { return this.eventData instanceof BirthEvent; }
  isBreedingEvent() { return this.eventData instanceof BreedingEvent; }
  isCalvingEvent() { return this.eventData instanceof CalvingEvent; }
  isChangeGroupEvent() { return this.eventData instanceof ChangeGroupEvent; }
  isDistressEvent() { return this.eventData instanceof DistressEvent; }
  isDryOffEvent() { return this.eventData instanceof DryOffEvent; }
  isHerdEntryEvent() { return this.eventData instanceof HerdEntryEvent; }
  isSystemHealthEvent() { return this.eventData instanceof SystemHealthEvent; }
  isSystemHeatEvent() { return this.eventData instanceof SystemHeatEvent; }
}
