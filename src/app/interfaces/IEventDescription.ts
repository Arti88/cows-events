export interface IEventDescription {
  comment: string;
  value: string|number|boolean|null;
}
