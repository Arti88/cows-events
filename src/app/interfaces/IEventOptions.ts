export interface IEventOptions {
  /* Common */
  eventId: number;
  type: string;
  animalId: string;
  cowId: number;
  deletable: boolean;
  lactationNumber: number;
  daysInLactation: number;
  ageInDays: number;
  startDateTime: number;
  reportingDateTime: number;

  /* ChangeGroup event type */
  newGroupId?: number;
  newGroupName?: string;
  currentGroupId?: number;
  currentGroupName?: string;

  /* SystemHeat event type */
  heatIndexPeak?: string;

  /* Distress event type */
  alertType?: string;
  duration?: number;
  originalStartDateTime?: number|null;
  endDateTime?: number|null;
  daysInPregnancy?: number|null;

  /* Breeding event type */
  sire?: unknown;
  breedingNumber?: number;
  isOutOfBreedingWindow?: boolean;
  interval?: number;

  /* System health event type */
  healthIndex?: number;
  endDate?: number;
  minValueDateTime?: number;

  /* Calving event type */
  destinationGroup?: number;
  destinationGroupName?: string;
  // daysInPregnancy?: number;
  oldLactationNumber?: number;
  newborns?: unknown;

  /* HerdEntry event type */
  // destinationGroup?: number;
  // destinationGroupName?: string|null;
  cowEntryStatus?: string;

  /* Birth event type */
  birthDateCalculated?: boolean;

  /* DryOff event type */
  // destinationGroup?: number;
  // destinationGroupName?: string;
  // daysInPregnancy?: number;
}
