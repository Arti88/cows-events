import {IEventOptions} from './IEventOptions';

export interface IEventsDataResponse {
  offset: number;
  limit: number;
  total: number;
  result: IEventOptions[];
}
