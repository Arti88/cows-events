import {IEventOptions} from '../interfaces/IEventOptions';
import {CommonEvent} from '../classes/CommonEvent';
import {EventTypes} from '../enums/EventTypes.enum';
import {BirthEvent} from '../classes/BirthEvent';
import {BreedingEvent} from '../classes/BreedingEvent';
import {CalvingEvent} from '../classes/CalvingEvent';
import {ChangeGroupEvent} from '../classes/ChangeGroupEvent';
import {DistressEvent} from '../classes/DistressEvent';
import {DryOffEvent} from '../classes/DryOffEvent';
import {HerdEntryEvent} from '../classes/HerdEntryEvent';
import {SystemHealthEvent} from '../classes/SystemHealthEvent';
import {SystemHeatEvent} from '../classes/SystemHeatEvent';

export class EventFactory {
  static createFromOptions(options: IEventOptions, createEmpty = false): CommonEvent|null {
    switch (options.type) {
      case EventTypes.BIRTH:
        return new BirthEvent(options, createEmpty);
      case EventTypes.BREEDING:
        return new BreedingEvent(options, createEmpty);
      case EventTypes.CALVING:
        return new CalvingEvent(options, createEmpty);
      case EventTypes.CHANGE_GROUP:
        return new ChangeGroupEvent(options, createEmpty);
      case EventTypes.DISTRESS:
        return new DistressEvent(options, createEmpty);
      case EventTypes.DRY_OFF:
        return new DryOffEvent(options, createEmpty);
      case EventTypes.HERD_ENTRY:
        return new HerdEntryEvent(options, createEmpty);
      case EventTypes.SYSTEM_HEALTH:
        return new SystemHealthEvent(options, createEmpty);
      case EventTypes.SYSTEM_HEAT:
        return new SystemHeatEvent(options, createEmpty);
      default:
    }
    console.warn(`>>> Unknown event type: ${options.type} within event ${options}`);
    return null;
  }
}
