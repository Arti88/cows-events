import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { EventsTableComponent } from './events-table/events-table.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import { EventModalComponent } from './event-modal/event-modal.component';
import { CreateEventModalComponent } from './create-event-modal/create-event-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    EventsTableComponent,
    EventModalComponent,
    CreateEventModalComponent
  ],
  imports: [
    NgbModalModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [EventModalComponent, CreateEventModalComponent],
})
export class AppModule { }
