import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {IEventsDataResponse} from './interfaces/IEventsDataResponse';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  constructor(
    private http: HttpClient,
  ) { }

  getData(): Observable<IEventsDataResponse> {
    return this.http.get<IEventsDataResponse>('assets/data.json');
  }
}
